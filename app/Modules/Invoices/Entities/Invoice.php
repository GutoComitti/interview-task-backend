<?php

namespace App\Modules\Invoices\Entities;

use App\Domain\Enums\StatusEnum;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Companies\Entities\Company;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Invoice extends Model
{
    protected $casts = [
        'id' => 'string',
        'date' => 'datetime',
        'due_date' => 'datetime'
    ];

    public function invoiceApprovalDto(): Attribute
    {
      return new Attribute(
        get: function ($value)
        {
            return new ApprovalDto($this->id, StatusEnum::tryFrom($this->status), self::class);
        }
      );
    }

    public function company(): BelongsTo
    {
      return $this->belongsTo(Company::class);
    }
}
