<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api;

use App\Modules\Invoices\Entities\Invoice;

interface InvoiceRepositoryInterface
{
    public function getInvoiceProducts(string $invoiceId): array;
    /**
     * @throws EntityNotFoundException
    */
    public function getInvoiceById(string $invoiceId): Invoice;
    public function rejectInvoice(string $invoiceId): void;
    public function approveInvoice(string $invoiceId): void;
}
