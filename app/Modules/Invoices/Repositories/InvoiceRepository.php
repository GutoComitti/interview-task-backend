<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Repositories;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Api\InvoiceRepositoryInterface;
use App\Modules\Invoices\Entities\Invoice;
use Illuminate\Contracts\Queue\EntityNotFoundException;
use Illuminate\Support\Facades\DB;

class InvoiceRepository implements InvoiceRepositoryInterface
{

    /**
     * @throws EntityNotFoundException
    */
    public function getInvoiceById(string $invoiceId): Invoice{
        $invoice = Invoice::find($invoiceId);
        if(is_null($invoice)){
            throw new EntityNotFoundException(Invoice::class, $invoiceId);
        }
        return $invoice;
    }

    private function setInvoiceStatus(string $invoiceId, StatusEnum $status):void {
        $invoice = $this->getInvoiceById($invoiceId);
        $invoice->status = $status;
        $invoice->save();
    }

    public function getInvoiceProducts(string $invoiceId):array {
        return DB::select("SELECT ipl.quantity,
                            p.name,
                            p.price,
                            (ipl.quantity * p.price) amount
                        from    invoice_product_lines ipl,
                                products p,
                                invoices i
                    WHERE i.id = ipl.invoice_id
                    AND ipl.product_id = p.id
                    AND i.id = ?",
                    [$invoiceId]);
    }

    public function rejectInvoice(string $invoiceId): void{
        $this->setInvoiceStatus($invoiceId, StatusEnum::REJECTED);
    }

    public function approveInvoice(string $invoiceId): void{
        $this->setInvoiceStatus($invoiceId, StatusEnum::APPROVED);
    }
}
