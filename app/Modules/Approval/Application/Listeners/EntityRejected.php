<?php

namespace App\Modules\Approval\Application\Listeners;

use App\Modules\Approval\Api\Events\EntityRejected as EntityRejectedEvent;
use App\Modules\Invoices\Repositories\InvoiceRepository;

class EntityRejected
{
  public function __construct()
  {
  }

  public function handle(EntityRejectedEvent $event): void
  {
    $invoiceRepository = new InvoiceRepository();
    $invoiceRepository->rejectInvoice($event->approvalDto->id);
  }
}
