<?php

namespace App\Modules\Approval\Application\Listeners;

use App\Modules\Approval\Api\Events\EntityApproved as EntityApprovedEvent;
use App\Modules\Invoices\Repositories\InvoiceRepository;

class EntityApproved
{
  public function __construct()
  {
  }

  public function handle(EntityApprovedEvent $event): void
  {
    $invoiceRepository = new InvoiceRepository();
    $invoiceRepository->approveInvoice($event->approvalDto->id);
  }
}
