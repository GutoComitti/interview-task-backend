<?php

declare(strict_types=1);

namespace App\Modules\Approval\Api\Dto;

use App\Domain\Enums\StatusEnum;

final readonly class ApprovalDto
{
    /** @param class-string $entity */
    public function __construct(
        public string $id,
        public StatusEnum $status,
        public string $entity,
    ) {
    }
}
