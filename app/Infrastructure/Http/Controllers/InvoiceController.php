<?php

namespace App\Infrastructure\Http\Controllers;

use App\Modules\Approval\Application\ApprovalFacade;
use App\Modules\Invoices\Repositories\InvoiceRepository;
use App\Modules\Invoices\Entities\Invoice;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Response;

class InvoiceController extends Controller
{

    public function show(Invoice $invoice): View
    {
        $invoiceRepository = new InvoiceRepository();
        $products = $invoiceRepository->getInvoiceProducts($invoice->id);
        $totalAmount = array_sum(array_column($products, 'amount'));
        $interval = $invoice->date->diff($invoice->due_date);
        $dueDays = $interval->format('%a');

        return view("invoice", [
            "invoice" => $invoice,
            "company" => config("company"),
            "products" => $products,
            "total_amount" => $totalAmount,
            "taxes_total" => $totalAmount * 0.0625,
            "dueDays" => $dueDays,
        ]);
    }

    public function approve(Invoice $invoice, ApprovalFacade $approvalFacade): Response
    {
        $approvalDto = $invoice->invoiceApprovalDto;
        $approvalFacade->approve($approvalDto);
        return response()->noContent();
    }

    public function reject(Invoice $invoice, ApprovalFacade $approvalFacade): Response
    {
        $approvalDto = $invoice->invoiceApprovalDto;
        $approvalFacade->reject($approvalDto);
        return response()->noContent();
    }
}
