<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Invoice</title>
</head>
<body>

    <div class="container">
        <div class="row font-weight-bold mb-3">
            <div class="col-6 text-left"><h3>{{ $company["name"] }}</h3></div>
            <div class="col-6 text-right"><h3>INVOICE</h3></div>
        </div>
        <div class="row">
            <div class="col text-left">{{ $company["street"] }}</div>
        </div>
        <div class="row">
            <div class="col text-left">{{ $company["city"] }}, {{ $company["zip"] }}</div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-4 text-left font-weight-bold">Bill to</div>
            <div class="col-4 text-left font-weight-bold">Ship to</div>
            <div class="col-2 text-right font-weight-bold">Invoice #</div>
            <div class="col-2 text-right">{{ $invoice->number }}</div>
        </div>
        <div class="row">
            <div class="col-4 text-left">{{ $invoice->company->name }}</div>
            <div class="col-4 text-left">{{ $invoice->company->name }}</div>
            <div class="col-2 text-right font-weight-bold">Invoice Date</div>
            <div class="col-2 text-right">{{ $invoice->date->format('m/d/Y') }}</div>
        </div>
        <div class="row">
            <div class="col-4 text-left">{{ $invoice->company->street }}</div>
            <div class="col-4 text-left">{{ $invoice->company->street }}</div>
            <div class="col-2 text-right font-weight-bold">P.O.#</div>
            <div class="col-2 text-right">{{-- //TODO: Confirm what P.O.# is! --}}</div>
        </div>
        <div class="row">
            <div class="col-4 text-left">{{ $invoice->company->city }}, {{ $invoice->company->zip }}</div>
            <div class="col-4 text-left">{{ $invoice->company->city }}, {{ $invoice->company->zip }}</div>
            <div class="col-2 text-right font-weight-bold">Due Date</div>
            <div class="col-2 text-right">{{ $invoice->due_date->format('m/d/Y') }}</div>
        </div>
        <table class="table mt-5">
            <thead>
              <tr>
                <th scope="col">QTY</th>
                <th scope="col">DESCRIPTION</th>
                <th scope="col">UNIT PRICE</th>
                <th scope="col">AMOUNT</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                    <td>{{ $product->quantity }}</td>
                    <td>{{ $product->name }}</td>
                    <td>@currency($product->price)</td>
                    <td>@currency($product->amount)</td>
                    </tr>
                @endforeach
              <tr>
                <td></td>
                <td></td>
                <td>Subtotal</td>
                <td>@currency($total_amount)</td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td>Sales Tax 6.25%</td>
                <td>@currency($taxes_total)</td>
              </tr>
              <tr class="font-weight-bold">
                <td></td>
                <td></td>
                <td>Total</td>
                <td>@currency($taxes_total + $total_amount)</td>
              </tr>
            </tbody>
          </table>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <div class="row">
              <div class="col text-left font-weight-bold">Terms and Conditions</div>
          </div>
          <div class="row">
              <div class="col text-left">Payment is due within {{ $dueDays }} days</div>
          </div>
          <div class="row">
              <div class="col text-left">Please make checks payable to: {{ $company["name"] }}</div>
          </div>
    </div>
</body>
</html>
