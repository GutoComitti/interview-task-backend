<?php

namespace Tests\Unit;

use App\Domain\Enums\StatusEnum;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class InvoiceTest extends TestCase
{
    use RefreshDatabase;

    private $approvedInvoice;
    private $rejectedInvoice;
    private $draftInvoice;

    protected function setUp(): void
    {
        parent::setUp();
        $this->draftInvoice = $this->createInvoice(1, StatusEnum::DRAFT);
        $this->approvedInvoice = $this->createInvoice(1, StatusEnum::APPROVED);
        $this->rejectedInvoice = $this->createInvoice(1, StatusEnum::REJECTED);
    }


    public function test_invoices_approval_works()
    {
        // TODO: Setup a draft invoice and call the function, afterwards check if it was approved;
    }

    public function test_invoices_rejection_works()
    {
        // TODO: Setup a draft invoice and call the function, afterwards check if it was rejected;
    }

    public function test_cannot_approve_non_draft_invoices()
    {
        // TODO: Setup an invoice with other status and see if it works; it shouldn't (business rule)
    }

    public function test_cannot_reject_non_draft_invoices()
    {
        // TODO: Setup an invoice with other status and see if it works; it shouldn't (business rule)
    }
}
