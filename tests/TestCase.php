<?php

namespace Tests;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Entities\Invoice;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function createInvoice($status = StatusEnum::DRAFT){
        // TODO: Creates an invoice with standard factory settings and the status passed;
    }
}
