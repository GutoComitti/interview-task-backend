<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class InvoiceTest extends TestCase
{
    use RefreshDatabase;

    public function test_invoice_page()
    {
        // TODO: setup an invoice and try to make a get request, see if it returns 200 and maybe some assertions regarding the content of the page

    }

    public function test_invoice_approval()
    {
        // TODO: setup a draft invoice, call the API route to approve passing the invoice ID and afterwards assert if no content and if the invoice status is now approved
    }

    public function test_invoice_rejection()
    {
        // TODO: setup a draft invoice, call the API route to reject passing the invoice ID and afterwards assert if no content and if the invoice status is now rejected

    }
}
