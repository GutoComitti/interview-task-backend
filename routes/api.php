<?php

use App\Infrastructure\Http\Controllers\InvoiceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/invoice/approve/{invoice}', [InvoiceController::class, 'approve']);
Route::post('/invoice/reject/{invoice}', [InvoiceController::class, 'reject']);
